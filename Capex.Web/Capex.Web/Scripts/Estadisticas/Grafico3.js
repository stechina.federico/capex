﻿//
//CHARTS CONFIGURACION GRAFICO 3
//

am4core.useTheme(am4themes_frozen);
am4core.useTheme(am4themes_animated);

var chart3 = am4core.create("chartdiv3", am4charts.XYChart);

// Create axes
var categoryAxis = chart3.xAxes.push(new am4charts.CategoryAxis());
categoryAxis.dataFields.category = "year";
categoryAxis.renderer.grid.template.location = 0;

var valueAxis = chart3.yAxes.push(new am4charts.ValueAxis());
valueAxis.renderer.inside = true;
valueAxis.renderer.labels.template.disabled = true;
valueAxis.min = 0;

// Create series
function createSeries(field, name) {

    // Set up series
    var series = chart3.series.push(new am4charts.ColumnSeries());
    series.name = name;
    series.dataFields.valueY = field;
    series.dataFields.categoryX = "year";
    series.sequencedInterpolation = true;

    // Make it stacked
    series.stacked = true;

    // Configure columns
    series.columns.template.width = am4core.percent(70);
    series.columns.template.tooltipText = "[bold]{name}[/]\n[font-size:14px]{categoryX}: {valueY}";

    // Add label
    var labelBullet = series.bullets.push(new am4charts.LabelBullet());
    labelBullet.label.text = "{valueY}";
    labelBullet.locationY = 0.5;

    return series;
}

createSeries("Rem", "Remanentes");
createSeries("Nuevo", "Nuevos");
createSeries("EX", "Extraordinarios");

// Legend
chart3.legend = new am4charts.Legend();





//
// OBTENER DATOS GRAFICO 2
//
$("#FNObtenerDatosGrafico3").click(function () {
    $.ajax({
        type: "GET",
        url: "Estadistica/ObtenerDatosGrafico3",
        dataType: "json",
        data: {
            
            IniPeriodo: AnnEjercicio3.val(),           
            EstToken: $('#Estado3').val(),         

        },
        success: function (resp) {
            chart3.data = resp
            console.log(resp)           
        },
        complete: function () {
            $.ajax({
                type: "GET",
                url: "Estadistica/ObtenerDatosGrafico3",
                dataType: "json",
                data: {

                    IniPeriodo: AnnEjercicio3.val() - 1,
                    EstToken: $('#Estado3').val(),
                },
                success: function (resp) {
                    chart4.data = resp
                    console.log(resp)
                }
            });
        }

    });

   
});



