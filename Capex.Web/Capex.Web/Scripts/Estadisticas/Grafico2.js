﻿//
//CHARTS CONFIGURACION GRAFICO 2
//


am4core.useTheme(am4themes_animated);

var chart2 = am4core.create("chartdiv2", am4charts.XYChart);

// Create axes
var categoryAxis = chart2.xAxes.push(new am4charts.CategoryAxis());
categoryAxis.dataFields.category = "month";
categoryAxis.renderer.grid.template.location = 0;

var valueAxis = chart2.yAxes.push(new am4charts.ValueAxis());
valueAxis.renderer.inside = true;
valueAxis.renderer.labels.template.disabled = true;
valueAxis.min = 0;

// Create series
function createSeries(field, name) {

    // Set up series
    var series = chart2.series.push(new am4charts.ColumnSeries());
    series.name = name;
    series.dataFields.valueY = field;
    series.dataFields.categoryX = "month";
    series.sequencedInterpolation = true;

    // Make it stacked
    series.stacked = false;

    // Configure columns
    series.columns.template.width = am4core.percent(70);
    series.columns.template.tooltipText = "[bold]{name}[/]\n[font-size:14px]{categoryX}: {valueY}";

    // Add label
    var labelBullet = series.bullets.push(new am4charts.LabelBullet());
    labelBullet.label.text = "{valueY}";
    labelBullet.locationY = 0.5;

    return series;
}

createSeries("PP", "CB - CD (Parcial)");
createSeries("PP", "Presupuesto (Parcial)");

// Legend
chart2.legend = new am4charts.Legend();

//
// OBTENER DATOS GRAFICO 2
//
$("#FNObtenerDatosGrafico2").click(function () {
    $.ajax({
        type: "GET",
        url: "Estadistica/ObtenerDatosGrafico2",
        dataType: "json",
        data: {
            AreaToken: Area2.val(),
            IniPeriodo: AnnEjercicio2.val(),
            NIAcronimo: Etapa2.val(),
            CSToken: Sso2.val(),
            EssToken: EstandarSeguridad2.val(),
            EstToken: Estado2.val(),
            CatToken: Categoria2.val(),
            EstadoProyecto: $('#EstadoProyecto2').val(),
            Clase: $('#Clase2').val(),
            Macrocategoria: $('#Macrocategoria2').val()

        },
        success: function (resp) {
            chart2.data = resp
            console.log(resp)
        }

    });
});


