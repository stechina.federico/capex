﻿//
//CHARTS CONFIGURACION GRAFICO 1
//
am4core.useTheme(am4themes_animated);

var chart = am4core.create("chartdiv1", am4charts.XYChart);

// Create axes
var categoryAxis = chart.xAxes.push(new am4charts.CategoryAxis());
categoryAxis.dataFields.category = "month";
categoryAxis.renderer.grid.template.location = 0;

var valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
valueAxis.renderer.inside = true;
valueAxis.renderer.labels.template.disabled = true;
valueAxis.min = 0;

// Create series
function createSeries(field, name) {

    // Set up series
    var series = chart.series.push(new am4charts.ColumnSeries());
    series.name = name;
    series.dataFields.valueY = field;
    series.dataFields.categoryX = "month";
    series.sequencedInterpolation = true;

    // Make it stacked
    series.stacked = true;

    // Configure columns
    series.columns.template.width = am4core.percent(70);
    series.columns.template.tooltipText = "[bold]{name}[/]\n[font-size:14px]{categoryX}: {valueY}";

    // Add label
    var labelBullet = series.bullets.push(new am4charts.LabelBullet());
    labelBullet.label.text = "{valueY}";
    labelBullet.locationY = 0.5;

    return series;
}

createSeries("nuevos", "Nuevos");
createSeries("remanentes", "Remanentes");

// Legend
chart.legend = new am4charts.Legend();


//
// OBTENER DATOS GRAFICO 1
//
$("#FNObtenerDatosGrafico1").click(function () {
    $.ajax({
        type: "GET",
        url: "Estadistica/ObtenerDatosGrafico1",
        dataType: "json",
        data: {
            AreaToken: Area.val(),
            IniPeriodo: AnnEjercicio.val(),
            NIAcronimo: Etapa.val(),
            CSToken: Sso.val(),
            EssToken: EstandarSeguridad.val(),
            EstToken: Estado.val(),
            CatToken: Categoria.val(),
            GerToken: AreaEjecutora.val(),
            Clase: $('#Clase').val(),
            Macrocategoria: $('#Macrocategoria').val()

        },
        success: function (resp) {
            chart.data = resp
            console.log(resp)
        }

    });
});

