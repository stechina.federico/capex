﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace Capex.Web.Controllers
{
    public class AuthorizeAdminOrMember : AuthorizeAttribute
    {
        public AuthorizeAdminOrMember()
        {
            Roles = "Administrator, Member";
        }
    }
    [AuthorizeAdminOrMember]
    public class PanelController : Controller
    {
        // GET: Panel
        public ActionResult Index()
        {
            if (!@User.Identity.IsAuthenticated)
            {
                return RedirectToAction("Logout", "Login");
            }
            return View();
        }
    }
   
}