﻿using Capex.Web.App_Start;
using CapexIdentity.Business;
using CapexIdentity.Infraestructure;
using CapexIdentity.Entities;
using CapexIdentity.Utilities;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Microsoft.Owin;

namespace Capex.Web.Controllers
{
    public class LoginController : Controller
    {
        /// <summary>
        /// 
        /// </summary>
        private ApplicationSignInManager _signInManager;
        private ApplicationUserManager _userManager;
        public ApplicationSignInManager SignInManager
        {
            get
            {
                return _signInManager ?? HttpContext.GetOwinContext().Get<ApplicationSignInManager>();
            }
            private set
            {
                _signInManager = value;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public ActionResult Logout()
        {
            Session.Abandon();
            SignInManager.AuthenticationManager.SignOut(DefaultAuthenticationTypes.ApplicationCookie);
            return RedirectToAction("Index", "Login");
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="returnUrl"></param>
        /// <returns></returns>
        [AllowAnonymous]
        public ActionResult Index(string returnUrl)
        {
            ViewBag.ReturnUrl = returnUrl;
            return View();
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="objLogin"></param>
        /// <param name="returnUrl"></param>
        /// <returns></returns>
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Index(bool chkRemember, LoginInfo objLogin, string returnUrl)
        {
            if (ModelState.IsValid)
            {
                ApplicationUser oUser = await SignInManager.UserManager.FindByNameAsync(objLogin.UserName);
                if (oUser != null && oUser.Password == objLogin.Password)
                {
                    switch (oUser.Status)
                    {
                        case EnumUserStatus.Pending:
                            ModelState.AddModelError("", "Error: User account has not been verified.");
                            break;
                        case EnumUserStatus.Active:
                            SignInManager.SignIn(oUser, false, false);
                            IList<string> roleList = UserRoleController.GetUserRoles(oUser.Id);
                            foreach(string role in roleList)
                            {
                                UserManager.AddToRole(oUser.Id, role);
                            }

                            if (string.IsNullOrEmpty(returnUrl))
                            {
                                if (chkRemember)
                                {
                                    HttpCookie cookie = new HttpCookie("_CAPEX_USERNAME_");
                                    cookie.Values.Add("UserName", objLogin.UserName);
                                    cookie.Expires = DateTime.Now.AddDays(15);
                                    Response.Cookies.Add(cookie);
                                }
                                return RedirectToAction("Index", "Panel");
                                //if (roleList.IndexOf("Administrator") >= 0)
                                //{
                                //    return RedirectToAction("Index", "Admin");
                                //}
                                //else
                                //{
                                //    return RedirectToAction("Index", "Member");
                                //}
                            }
                            return RedirectToLocal(returnUrl);

                        case EnumUserStatus.Banned:
                            ModelState.AddModelError("", "Error: User account has been banned.");
                            break;
                        case EnumUserStatus.LockedOut:
                            ModelState.AddModelError("", "Error: User account has been locked out due to multiple login tries.");
                            break;
                    }
                }
                else
                {
                    ModelState.AddModelError("", "Error: Invalid login details.");
                }
            }
            return View(objLogin);
        }

        private ActionResult RedirectToLocal(string returnUrl)
        {
            if (Url.IsLocalUrl(returnUrl))
            {
                return Redirect(returnUrl);
            }
            return RedirectToAction("Index", "Login");
        }
    }
}