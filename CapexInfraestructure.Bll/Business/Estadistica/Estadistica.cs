﻿using CapexInfraestructure.Bll.Entities.Estadistica;
using CapexInfraestructure.Utilities;
using Dapper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;


namespace CapexInfraestructure.Bll.Business.Estadistica
{
    public class Estadistica : IEstadistica
    {
        /* ------------------------------------------------------------------------------------
         * 
         * PMO360
         * 
         * -----------------------------------------------------------------------------------
         * 
         * CLIENTE          : 
         * PRODUCTO         : CAPEX
         * RESPONABILIDAD   : PROVEER OPERACIONES Y LOGICA DE NEGOCIO PARA EL MODULO DE EJERCICIO DE PLANIFICACION
         * TIPO             : LOGICA DE NEGOCIO
         * DESARROLLADO POR : PMO360
         * FECHA            : 2018
         * VERSION          : 0.0.1
         * PROPOSITO        : WRAPPER DE OPERACIONES A LA BASE DE DATOS /REPOSITORIO
         * 
         * 
         */
        #region "PROPIEDADES"
        public string ExceptionResult { get; set; }
        public string AppModule { get; set; }
        #endregion

        #region "GLOBALS"
        public SqlConnection ORM;
        #endregion

        #region "CONSTRUCTOR"
        public Estadistica()
        {
            AppModule = "Estadística";
            ORM = Utils.Conectar();
        }
        #endregion

        #region "METODOS ESTADISTICA"
        /// <summary>
        /// OBTENER DATOS GRAFICO 1
        /// </summary>
        /// <param name="filtro"></param>
        /// <returns></returns>
        /// 
        public List<EstadisticaModel.Grafico1DTO> ObtenerDatosGrafico1(FiltroEstadistica.Grafico1 filtro)
        {
            try
            {
                var query = ORM.Query<EstadisticaModel.Grafico1Data>("CAPEX_SEL_ESTADISTICA_GRAFICO1", filtro, commandType: CommandType.StoredProcedure).ToList();

                List<EstadisticaModel.Grafico1IniNuevos> listNuevos = new List<EstadisticaModel.Grafico1IniNuevos>();
                List<EstadisticaModel.Grafico1IniRemanentes> listRemanentes = new List<EstadisticaModel.Grafico1IniRemanentes>();
                foreach (var item in query)
                {
                    if (item.IniTipoEjercicio == "CREAR")
                    {
                        #region "NUEVOS"
                        var data = new EstadisticaModel.Grafico1IniNuevos()

                        { month = "Ene", nuevos = item.Ene };
                        listNuevos.Add(data);

                        data = new EstadisticaModel.Grafico1IniNuevos() { month = "Feb", nuevos = item.Feb };
                        listNuevos.Add(data);

                        data = new EstadisticaModel.Grafico1IniNuevos() { month = "Mar", nuevos = item.Mar };
                        listNuevos.Add(data);

                        data = new EstadisticaModel.Grafico1IniNuevos() { month = "Abr", nuevos = item.Abr };
                        listNuevos.Add(data);

                        data = new EstadisticaModel.Grafico1IniNuevos() { month = "May", nuevos = item.May };
                        listNuevos.Add(data);

                        data = new EstadisticaModel.Grafico1IniNuevos() { month = "Jun", nuevos = item.Jun };
                        listNuevos.Add(data);

                        data = new EstadisticaModel.Grafico1IniNuevos() { month = "Jul", nuevos = item.Jul };
                        listNuevos.Add(data);

                        data = new EstadisticaModel.Grafico1IniNuevos() { month = "Ago", nuevos = item.Ago };
                        listNuevos.Add(data);

                        data = new EstadisticaModel.Grafico1IniNuevos() { month = "Sep", nuevos = item.Sep };
                        listNuevos.Add(data);

                        data = new EstadisticaModel.Grafico1IniNuevos() { month = "Oct", nuevos = item.Oct };
                        listNuevos.Add(data);

                        data = new EstadisticaModel.Grafico1IniNuevos() { month = "Nov", nuevos = item.Nov };
                        listNuevos.Add(data);

                        data = new EstadisticaModel.Grafico1IniNuevos() { month = "Dic", nuevos = item.Dic };
                        listNuevos.Add(data);

                        #endregion
                    }

                    else if (item.IniTipoEjercicio == "IMPORTAR")
                    {
                        #region "REMANENTES"
                        var data = new EstadisticaModel.Grafico1IniRemanentes()

                        { month = "Ene", remanentes = item.Ene };
                        listRemanentes.Add(data);

                        data = new EstadisticaModel.Grafico1IniRemanentes() { month = "Feb", remanentes = item.Feb };
                        listRemanentes.Add(data);

                        data = new EstadisticaModel.Grafico1IniRemanentes() { month = "Mar", remanentes = item.Mar };
                        listRemanentes.Add(data);

                        data = new EstadisticaModel.Grafico1IniRemanentes() { month = "Abr", remanentes = item.Abr };
                        listRemanentes.Add(data);

                        data = new EstadisticaModel.Grafico1IniRemanentes() { month = "May", remanentes = item.May };
                        listRemanentes.Add(data);

                        data = new EstadisticaModel.Grafico1IniRemanentes() { month = "Jun", remanentes = item.Jun };
                        listRemanentes.Add(data);

                        data = new EstadisticaModel.Grafico1IniRemanentes() { month = "Jul", remanentes = item.Jul };
                        listRemanentes.Add(data);

                        data = new EstadisticaModel.Grafico1IniRemanentes() { month = "Ago", remanentes = item.Ago };
                        listRemanentes.Add(data);

                        data = new EstadisticaModel.Grafico1IniRemanentes() { month = "Sep", remanentes = item.Sep };
                        listRemanentes.Add(data);

                        data = new EstadisticaModel.Grafico1IniRemanentes() { month = "Oct", remanentes = item.Oct };
                        listRemanentes.Add(data);

                        data = new EstadisticaModel.Grafico1IniRemanentes() { month = "Nov", remanentes = item.Nov };
                        listRemanentes.Add(data);

                        data = new EstadisticaModel.Grafico1IniRemanentes() { month = "Dic", remanentes = item.Dic };
                        listRemanentes.Add(data);

                        #endregion
                    }
                };

                var result = from lNuevos in listNuevos
                             join lRemanentes in listRemanentes on lNuevos.month equals lRemanentes.month
                             select new EstadisticaModel.Grafico1DTO
                             {
                                 month = lNuevos.month,
                                 remanentes = lRemanentes.remanentes,
                                 nuevos = lNuevos.nuevos
                             };


                return result.ToList();


            }
            catch (Exception err)
            {
                ExceptionResult = AppModule + "Obtener Datos Grafico 1, Mensaje: " + err.Message.ToString() + "-" + ", Detalle: " + err.StackTrace.ToString();
                Utils.LogError(ExceptionResult);
                return null;
            }
        }

        /// <summary>
        /// OBTENER DATOS GRAFICO 2
        /// </summary>
        /// <param name="filtro"></param>
        /// <returns></returns>
        /// 
        public List<EstadisticaModel.Grafico2DTO> ObtenerDatosGrafico2(FiltroEstadistica.Grafico2 filtro)
        {
            try
            {
                var query = ORM.Query<EstadisticaModel.Grafico2Data>("CAPEX_SEL_ESTADISTICA_GRAFICO2", filtro, commandType: CommandType.StoredProcedure).ToList();

                List<EstadisticaModel.Grafico2IniCB> listCB = new List<EstadisticaModel.Grafico2IniCB>();
                List<EstadisticaModel.Grafico2IniPP> listPP = new List<EstadisticaModel.Grafico2IniPP>();
                foreach (var item in query)
                {
                    if (item.IniTipo == "CB")
                    {
                        #region "CB"
                        var data = new EstadisticaModel.Grafico2IniCB()

                        { month = "Ene", CB = item.Ene };
                        listCB.Add(data);

                        data = new EstadisticaModel.Grafico2IniCB() { month = "Feb", CB = item.Feb };
                        listCB.Add(data);

                        data = new EstadisticaModel.Grafico2IniCB() { month = "Mar", CB = item.Mar };
                        listCB.Add(data);

                        data = new EstadisticaModel.Grafico2IniCB() { month = "Abr", CB = item.Abr };
                        listCB.Add(data);

                        data = new EstadisticaModel.Grafico2IniCB() { month = "May", CB = item.May };
                        listCB.Add(data);

                        data = new EstadisticaModel.Grafico2IniCB() { month = "Jun", CB = item.Jun };
                        listCB.Add(data);

                        data = new EstadisticaModel.Grafico2IniCB() { month = "Jul", CB = item.Jul };
                        listCB.Add(data);

                        data = new EstadisticaModel.Grafico2IniCB() { month = "Ago", CB = item.Ago };
                        listCB.Add(data);

                        data = new EstadisticaModel.Grafico2IniCB() { month = "Sep", CB = item.Sep };
                        listCB.Add(data);

                        data = new EstadisticaModel.Grafico2IniCB() { month = "Oct", CB = item.Oct };
                        listCB.Add(data);

                        data = new EstadisticaModel.Grafico2IniCB() { month = "Nov", CB = item.Nov };
                        listCB.Add(data);

                        data = new EstadisticaModel.Grafico2IniCB() { month = "Dic", CB = item.Dic };
                        listCB.Add(data);

                        #endregion
                    }

                    else if (item.IniTipo == "PP")
                    {
                        #region "PP"
                        var data = new EstadisticaModel.Grafico2IniPP()

                        { month = "Ene", PP = item.Ene };
                        listPP.Add(data);

                        data = new EstadisticaModel.Grafico2IniPP() { month = "Feb", PP = item.Feb };
                        listPP.Add(data);

                        data = new EstadisticaModel.Grafico2IniPP() { month = "Mar", PP = item.Mar };
                        listPP.Add(data);

                        data = new EstadisticaModel.Grafico2IniPP() { month = "Abr", PP = item.Abr };
                        listPP.Add(data);

                        data = new EstadisticaModel.Grafico2IniPP() { month = "May", PP = item.May };
                        listPP.Add(data);

                        data = new EstadisticaModel.Grafico2IniPP() { month = "Jun", PP = item.Jun };
                        listPP.Add(data);

                        data = new EstadisticaModel.Grafico2IniPP() { month = "Jul", PP = item.Jul };
                        listPP.Add(data);

                        data = new EstadisticaModel.Grafico2IniPP() { month = "Ago", PP = item.Ago };
                        listPP.Add(data);

                        data = new EstadisticaModel.Grafico2IniPP() { month = "Sep", PP = item.Sep };
                        listPP.Add(data);

                        data = new EstadisticaModel.Grafico2IniPP() { month = "Oct", PP = item.Oct };
                        listPP.Add(data);

                        data = new EstadisticaModel.Grafico2IniPP() { month = "Nov", PP = item.Nov };
                        listPP.Add(data);

                        data = new EstadisticaModel.Grafico2IniPP() { month = "Dic", PP = item.Dic };
                        listPP.Add(data);

                        #endregion
                    }
                };

                var result = from lCB in listCB
                             join lPP in listPP on lCB.month equals lPP.month
                             select new EstadisticaModel.Grafico2DTO
                             {
                                 month = lCB.month,
                                 PP = lPP.PP,
                                 CB = lCB.CB
                             };


                return result.ToList();


            }
            catch (Exception err)
            {
                ExceptionResult = AppModule + "Obtener Datos Grafico 2, Mensaje: " + err.Message.ToString() + "-" + ", Detalle: " + err.StackTrace.ToString();
                Utils.LogError(ExceptionResult);
                return null;
            }
        }




        /// <summary>
        /// OBTENER DATOS GRAFICO 3
        /// </summary>
        /// <param name="filtro"></param>
        /// <returns></returns>
        /// 
        public List<EstadisticaModel.Grafico3DTO> ObtenerDatosGrafico3(FiltroEstadistica.Grafico3 filtro)
        {
            try
            {
                var query = ORM.Query<EstadisticaModel.Grafico3Data>("CAPEX_SEL_ESTADISTICA_GRAFICO3", filtro, commandType: CommandType.StoredProcedure).ToList();

                List<EstadisticaModel.Grafico3TipoNuevo> listNuevo = new List<EstadisticaModel.Grafico3TipoNuevo>();
                List<EstadisticaModel.Grafico3TipoRem> listRem = new List<EstadisticaModel.Grafico3TipoRem>();
                List<EstadisticaModel.Grafico3TipoEX> listEX = new List<EstadisticaModel.Grafico3TipoEX>();

                foreach (var item in query)
                {
                    if (item.IniTipo == "PP" && item.IniTipoEjercicio == "CREAR")
                    {
                        var data = new EstadisticaModel.Grafico3TipoNuevo()
                        {
                            year = item.IniPeriodo,
                            IniTipoEjercicio = item.IniTipoEjercicio,
                            Nuevo = item.Ene + item.Feb + item.Mar + item.Abr + item.May + item.Jun + item.Jul +
                            item.Ago + item.Sep + item.Oct + item.Nov + item.Dic
                        };
                        listNuevo.Add(data);
                    }

                    else if (item.IniTipo == "PP" && item.IniTipoEjercicio == "IMPORTAR")
                    {
                        var data = new EstadisticaModel.Grafico3TipoRem()
                        {
                            year = item.IniPeriodo,
                            IniTipoEjercicio = item.IniTipoEjercicio,
                            Rem = item.Ene + item.Feb + item.Mar + item.Abr + item.May + item.Jun + item.Jul +
                               item.Ago + item.Sep + item.Oct + item.Nov + item.Dic
                        };
                        listRem.Add(data);
                    }

                    else if (item.IniTipo == "EX")
                    {
                        var data = new EstadisticaModel.Grafico3TipoEX()
                        {
                            year = item.IniPeriodo,
                            EX = item.Ene + item.Feb + item.Mar + item.Abr + item.May + item.Jun + item.Jul +
                               item.Ago + item.Sep + item.Oct + item.Nov + item.Dic
                        };
                        listEX.Add(data);
                    }
                };

                var result = from lNuevo in listNuevo
                             join lRem in listRem on lNuevo.year equals lRem.year into leftRem
                             join lEX in listEX on lNuevo.year equals lEX.year into leftEX
                             from EXLeft in leftEX.DefaultIfEmpty()
                             from RemLeft in leftRem.DefaultIfEmpty()
                             select new EstadisticaModel.Grafico3DTO
                             {
                                 year = lNuevo.year,                                
                                 Nuevo = lNuevo?.Nuevo,
                                 Rem = RemLeft?.Rem,
                                 EX = EXLeft?.EX
                             };


                return result.ToList();


            }
            catch (Exception err)
            {
                ExceptionResult = AppModule + "Obtener Datos Grafico 3, Mensaje: " + err.Message.ToString() + "-" + ", Detalle: " + err.StackTrace.ToString();
                Utils.LogError(ExceptionResult);
                return null;
            }
        }




        /// <summary>
        /// OBTENER DATOS GRAFICO 4 - ESTADOS
        /// </summary>
        /// <param name="filtro"></param> Para evitar redundancia se reutilizan filtros del grafico 2
        /// <returns></returns>
        /// 
        public List<EstadisticaModel.Grafico4DTO> ObtenerDatosGrafico4(FiltroEstadistica.Grafico2 filtro) 
        {
            try
            {
                var query = ORM.Query<EstadisticaModel.Grafico4Data>("CAPEX_SEL_ESTADISTICA_GRAFICO4_ESTADO", filtro, commandType: CommandType.StoredProcedure).ToList();
                           
                var result = from data in query                            
                             select new EstadisticaModel.Grafico4DTO
                             {
                                 year = data.years,
                                 tipo = data.tipo,
                                 totales = data.totales
                             };

                return result.ToList();
                
            }
            catch (Exception err)
            {
                ExceptionResult = AppModule + "Obtener Datos Grafico 4 - Estados, Mensaje: " + err.Message.ToString() + "-" + ", Detalle: " + err.StackTrace.ToString();
                Utils.LogError(ExceptionResult);
                return null;
            }
        }


        /// <summary>
        /// OBTENER DATOS GRAFICO 4 - CATEGORIAS
        /// </summary>
        /// <param name="filtro"></param> Para evitar redundancia se reutilizan filtros del grafico 2
        /// <returns></returns>
        /// 
        public List<EstadisticaModel.Grafico4DTO_Categoria> ObtenerDatosGrafico4_Categoria(FiltroEstadistica.Grafico2 filtro) 
        {
            try
            {
                var query = ORM.Query<EstadisticaModel.Grafico4DTO_Categoria>("CAPEX_SEL_ESTADISTICA_GRAFICO4_CATEGORIA", filtro, commandType: CommandType.StoredProcedure).ToList();

                var result = from data in query
                             select new EstadisticaModel.Grafico4DTO_Categoria
                             {
                                 Categoria = data.Categoria,                                
                                 Total = data.Total
                             };

                return result.ToList();

            }
            catch (Exception err)
            {
                ExceptionResult = AppModule + "Obtener Datos Grafico 4 - Categorias, Mensaje: " + err.Message.ToString() + "-" + ", Detalle: " + err.StackTrace.ToString();
                Utils.LogError(ExceptionResult);
                return null;
            }
        }



        /// <summary>
        /// OBTENER DATOS AREA CLIENTE
        /// </summary>
        /// <param name="token"></param>
        /// <returns></returns>
        /// 
        public List<FiltroEstadistica.AreaCliente> ListarAreaCliente(string token)
        {
            try
            {
                return ORM.Query<FiltroEstadistica.AreaCliente>("CAPEX_SEL_AREAS", commandType: CommandType.StoredProcedure).ToList();
            }
            catch (Exception err)
            {
                ExceptionResult = AppModule + "Obtener Datos Area Cliente, Mensaje: " + err.Message.ToString() + "-" + ", Detalle: " + err.StackTrace.ToString();
                Utils.LogError(ExceptionResult);
                return null;
            }
        }

        /// <summary>
        /// OBTENER DATOS AÑO EJERCICIO
        /// </summary>
        /// <param name="token"></param>
        /// <returns></returns>
        /// 
        public List<FiltroEstadistica.AnnEjercicio> ListarAnnEjercicio(string token)
        {
            try
            {
                return ORM.Query<FiltroEstadistica.AnnEjercicio>("CAPEX_SEL_INICIATIVA", commandType: CommandType.StoredProcedure).ToList();
            }
            catch (Exception err)
            {
                ExceptionResult = AppModule + "Obtener Datos Año Ejercicio, Mensaje: " + err.Message.ToString() + "-" + ", Detalle: " + err.StackTrace.ToString();
                Utils.LogError(ExceptionResult);
                return null;
            }
        }


        /// <summary>
        /// OBTENER DATOS ETAPAS
        /// </summary>
        /// <param name="token"></param>
        /// <returns></returns>
        /// 
        public List<FiltroEstadistica.Etapas> ListarEtapas(string token)
        {
            try
            {
                return ORM.Query<FiltroEstadistica.Etapas>("CAPEX_SEL_ETAPAS", commandType: CommandType.StoredProcedure).ToList();
            }
            catch (Exception err)
            {
                ExceptionResult = AppModule + "Obtener Datos Etapas, Mensaje: " + err.Message.ToString() + "-" + ", Detalle: " + err.StackTrace.ToString();
                Utils.LogError(ExceptionResult);
                return null;
            }
        }


        /// <summary>
        /// OBTENER ESTANDAR C SSO
        /// </summary>
        /// <param name="token"></param>
        /// <returns></returns>
        /// 
        public List<FiltroEstadistica.SSO> ListarSSO(string token)
        {
            try
            {
                return ORM.Query<FiltroEstadistica.SSO>("CAPEX_SEL_CLASIFICACION_SSO", commandType: CommandType.StoredProcedure).ToList();
            }
            catch (Exception err)
            {
                ExceptionResult = AppModule + "Obtener Datos SSO, Mensaje: " + err.Message.ToString() + "-" + ", Detalle: " + err.StackTrace.ToString();
                Utils.LogError(ExceptionResult);
                return null;
            }
        }





        /// <summary>
        /// OBTENER ESTANDAR SEGURIDAD
        /// </summary>
        /// <param name="token"></param>
        /// <returns></returns>
        /// 
        public List<FiltroEstadistica.EstandarSeguridad> ListarEstandarSeguridad(string token)
        {
            try
            {
                return ORM.Query<FiltroEstadistica.EstandarSeguridad>("CAPEX_SEL_ESTANDAR_SEGURIDAD_V2", commandType: CommandType.StoredProcedure).ToList();
            }
            catch (Exception err)
            {
                ExceptionResult = AppModule + "Obtener Estandar Seguridad, Mensaje: " + err.Message.ToString() + "-" + ", Detalle: " + err.StackTrace.ToString();
                Utils.LogError(ExceptionResult);
                return null;
            }
        }



        /// <summary>
        /// OBTENER CATEGORIAS
        /// </summary>
        /// <param name="token"></param>
        /// <returns></returns>
        /// 
        public List<FiltroEstadistica.Categorias> ListarCategorias(string token)
        {
            try
            {
                return ORM.Query<FiltroEstadistica.Categorias>("CAPEX_SEL_CATEGORIAS", commandType: CommandType.StoredProcedure).ToList();
            }
            catch (Exception err)
            {
                ExceptionResult = AppModule + "Obtener Categorias, Mensaje: " + err.Message.ToString() + "-" + ", Detalle: " + err.StackTrace.ToString();
                Utils.LogError(ExceptionResult);
                return null;
            }
        }




        /// <summary>
        /// OBTENER ESTADOS INICIATIVA
        /// </summary>
        /// <param name="token"></param>
        /// <returns></returns>
        /// 
        public List<FiltroEstadistica.EstadoIniciativa> ListarEstadoIniciativa(string token)
        {
            try
            {
                return ORM.Query<FiltroEstadistica.EstadoIniciativa>("CAPEX_SEL_ESTADO_INICIATIVA", commandType: CommandType.StoredProcedure).ToList();
            }
            catch (Exception err)
            {
                ExceptionResult = AppModule + "Obtener Categorias, Mensaje: " + err.Message.ToString() + "-" + ", Detalle: " + err.StackTrace.ToString();
                Utils.LogError(ExceptionResult);
                return null;
            }
        }


        /// <summary>
        /// OBTENER AREA EJECUTORA
        /// </summary>
        /// <param name="token"></param>
        /// <returns></returns>
        /// 
        public List<FiltroEstadistica.AreaEjecutora> ListarAreaEjecutora(string token)
        {
            try
            {
                return ORM.Query<FiltroEstadistica.AreaEjecutora>("CAPEX_SEL_GERENCIAS", commandType: CommandType.StoredProcedure).ToList();
            }
            catch (Exception err)
            {
                ExceptionResult = AppModule + "Obtener Area Ejecutora, Mensaje: " + err.Message.ToString() + "-" + ", Detalle: " + err.StackTrace.ToString();
                Utils.LogError(ExceptionResult);
                return null;
            }
        }



        #endregion "METODOS IDENTIFICACION"

        #region "GLOBALES"
        /// <summary>
        /// REGISTRAR  ARCHIVOS EN DB
        /// </summary>
        /// <param name="IniToken"></param>
        /// <param name="ParUsuario"></param>
        /// <param name="ParNombre"></param>
        /// <param name="ParPaso"></param>
        /// <param name="ParCaso"></param>
        /// <returns></returns>
        public string RegistrarArchivo(string IniToken, string ParUsuario, string ParNombre, string ParPaso, string ParCaso)
        {
            try
            {
                ORM.Query("CAPEX_INS_REGISTRAR_ARCHIVO", new { IniToken, ParUsuario, ParNombre, ParPaso, ParCaso }, commandType: CommandType.StoredProcedure).SingleOrDefault();
                return "Registrado";
            }
            catch (Exception exc)
            {
                ExceptionResult = AppModule + "RegistrarArchivo, Mensaje: " + exc.Message.ToString() + "-" + ", Detalle: " + exc.StackTrace.ToString();
                Utils.LogError(ExceptionResult);
                return "Error";
            }
        }

        #endregion
    }

}
