﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CapexInfraestructure.Bll.Entities.Estadistica
{
    public class EstadisticaModel
    {
        public class Grafico1Data
        {
           
            public string IniTipoEjercicio { get; set; }
            public int Ene { get; set; }
            public int Feb { get; set; }
            public int Mar { get; set; }
            public int Abr { get; set; } 
            public int May { get; set; }
            public int Jun { get; set; }
            public int Jul { get; set; }
            public int Ago { get; set; }
            public int Sep { get; set; }
            public int Oct { get; set; }
            public int Nov { get; set; }
            public int Dic { get; set; }

        }
        public class Grafico1DTO
        {
            public string month { get; set; }
            public int nuevos { get; set; }
            public int remanentes { get; set; }
           
          
        }
        public class Grafico1IniNuevos
        {
            public string month { get; set; }         
            public int nuevos { get; set; }

        }
        public class Grafico1IniRemanentes
        {
            public string month { get; set; }
            public int remanentes { get; set; }

        }


        public class Grafico2Data
        {
           
            public string IniTipo { get; set; }
            public int Ene { get; set; }
            public int Feb { get; set; }
            public int Mar { get; set; }
            public int Abr { get; set; }
            public int May { get; set; }
            public int Jun { get; set; }
            public int Jul { get; set; }
            public int Ago { get; set; }
            public int Sep { get; set; }
            public int Oct { get; set; }
            public int Nov { get; set; }
            public int Dic { get; set; }

        }
        public class Grafico2DTO
        {
            public string month { get; set; }
            public int CB { get; set; }
            public int PP { get; set; }


        }
        public class Grafico2IniCB
        {
            public string month { get; set; }
            public int CB { get; set; }

        }
        public class Grafico2IniPP
        {
            public string month { get; set; }
            public int PP { get; set; }

        }

        public class Grafico3Data
        {
            public string IniPeriodo { get; set; }
            public string IniTipo { get; set; }
            public string IniTipoEjercicio { get; set; }
            public int Ene { get; set; }
            public int Feb { get; set; }
            public int Mar { get; set; }
            public int Abr { get; set; }
            public int May { get; set; }
            public int Jun { get; set; }
            public int Jul { get; set; }
            public int Ago { get; set; }
            public int Sep { get; set; }
            public int Oct { get; set; }
            public int Nov { get; set; }
            public int Dic { get; set; }

        }
        public class Grafico3DTO
        {
            public string year { get; set; }
            public int? Nuevo { get; set; }
            public int? Rem { get; set; }
            public int? EX { get; set; }

        }
        public class Grafico3TipoNuevo
        {
            public string year { get; set; }
            public int Nuevo { get; set; }
            public string IniTipoEjercicio { get; set; }

        }
        public class Grafico3TipoRem
        {
            public string year { get; set; }
            public int Rem { get; set; }
            public string IniTipoEjercicio { get; set; }

        }
        public class Grafico3TipoEX
        {
            public string year { get; set; }
            public int EX { get; set; }

        }


        public class Grafico4Data
        {
            public string years { get; set; }
            public string tipo { get; set; }
            public int totales { get; set; }
        }

        public class Grafico4DTO
        {
            public string year { get; set; }
            public string tipo { get; set; }
            public int totales { get; set; }

        }

        public class Grafico4DTO_Categoria
        {
            public string Categoria { get; set; }
         
            public int Total { get; set; }

        }
    }

}
